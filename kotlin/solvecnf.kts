/*
 * Copyright (c) 2019 Venkatesh-Prasad Ranganath
 *
 * BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath (rvprasad)
 */

@file:CompilerOpts("-jvm-target 1.8")
@file:DependsOn("com.github.ajalt:clikt:2.8.0")
@file:DependsOn("org.jgrapht:jgrapht-core:1.3.0")
// @file:KotlinOpts("-J-ea")

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.types.int
import org.jgrapht.alg.connectivity.KosarajuStrongConnectivityInspector
import org.jgrapht.graph.DefaultDirectedGraph
import org.jgrapht.graph.DefaultEdge
import java.io.File
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.atomic.AtomicInteger
import java.util.stream.IntStream
import kotlin.math.abs
import kotlin.math.absoluteValue
import kotlin.text.Regex

private typealias Assignment = Set<Int>
private typealias Clause = List<Int>
private typealias Var = UInt

internal class CommandLine: CliktCommand(name = "kscript solvecnf.kts",
        printHelpOnEmptyArgs = true,
        help="Adaptation of Randomized CNF solver based on DEL-PPZ algorithm by Ghosh and Misra") {
    private val cnf by option("-c", "--cnf", help = "CNF file in DIMACS format " +
            "(http://www.satcompetition.org/2009/format-benchmarks2009.html)").required()
    private val maxTries by option("-m", "--maxTries", help = "Number of " +
            "tries of the randomized algorithm. (Default: #vars * #clauses)").int()

    override fun run() {
        val (clauses, vars) = readClauses(cnf)
        val mTries = maxTries?.absoluteValue ?: clauses.size
        RandomizedCNFSolver.solve(clauses, vars.toList(), mTries).run {
            if (isEmpty()) {
                println("s UNSATISFIABLE")
            } else {
                println("s SATISFIABLE")
                println("v ${sortedBy(::abs).joinToString(" ")} 0")
            }
        }
    }

    private fun readClauses(fileName: String): Pair<Collection<Clause>, Set<Var>> {
        var expectedNumVars = 0
        var expectedNumClauses = 0
        val infoRegex = Regex("^p cnf \\d+ \\d+.*")
        val clauseRegex = Regex("^\\s*-?\\d.*")
        fun tokenize(line: String) = line.trim().split(' ').filter { it.isNotBlank() }

        val clauses = File(fileName).bufferedReader().lineSequence().map { line ->
            val tokens = tokenize(line)
            when {
                infoRegex.containsMatchIn(line) -> {
                    expectedNumVars = Integer.parseInt(tokens[2])
                    expectedNumClauses = Integer.parseInt(tokens[3])
                    null
                }
                clauseRegex.containsMatchIn(line) -> tokens.map { it.toInt() }.takeWhile { it != 0 }.toMutableList()
                else -> null
            }
        }.filterNotNull().toList()

        val seenVars = clauses.flatten().map { it.absoluteValue.toUInt() }.toSet()
        println("c Variables ${seenVars.size} seen $expectedNumVars expected")
        println("c Clauses ${clauses.size} seen $expectedNumClauses expected")

        return Pair(clauses, seenVars)
    }
}

private object RandomizedCNFSolver {
    private fun get2CNFByLiteralDeletion(clauses: Collection<Clause>) =
            clauses.map { (if (it.size < 3) it else it.shuffled().slice(0..1)).toMutableList() }.toMutableList()

    fun solve(givenClauses:Collection<Clause>, vars: List<Var>, tries:Int): Set<Int> {
        val basicAssignment = vars.map(Var::toInt)
        if (givenClauses.any { it.isEmpty() }) {
            println("c repetitions 0")
            return emptySet()
        } else if (givenClauses.isEmpty()) {
            println("c repetitions 0")
            return basicAssignment.toSet()
        }

        val reps = AtomicInteger()
        val numOfVars = vars.size
        val ret = IntStream.range(0, tries).parallel().mapToObj {
            reps.getAndIncrement()

            val random = ThreadLocalRandom.current()
            val (_, assignment) = (0..(numOfVars - 1)).shuffled().fold(Pair(givenClauses, emptySet<Int>())) { acc, i ->
                val (clauses, assignment) = acc
                val v = basicAssignment[i]
                if (clauses.isEmpty() || v in assignment || -v in assignment)
                    acc

                val twoCNF = get2CNFByLiteralDeletion(clauses)
                val assignmentFrom2CNF = SCCBased2CNFSolver.solve(twoCNF)
                if (assignmentFrom2CNF.isNotEmpty())
                    Pair(emptySet<Clause>(), assignment + assignmentFrom2CNF)

                val verdict = SCCBased2CNFSolver.performBCP(clauses)
                when (verdict) {
                    is SCCBased2CNFSolver.Verdict.Conflicts -> Pair(emptySet(), emptySet())
                    is SCCBased2CNFSolver.Verdict.Satisfied -> Pair(emptySet(), assignment + verdict.assignment)
                    is SCCBased2CNFSolver.Verdict.Unresolved -> {
                        val newAssignment1 = assignment + verdict.partialAssignment
                        if (verdict.clauses.any { v in it || -v in it }) {
                            val polarity = if (random.nextBoolean()) 1 else -1
                            val newAssignment = newAssignment1 + v * polarity
                            Pair(SCCBased2CNFSolver.simplifyClausesBasedOnValues(clauses, newAssignment),
                                    newAssignment)
                        } else
                            Pair(verdict.clauses, newAssignment1)
                    }
                }
            }

            if (givenClauses.all { c -> c.any { it in assignment } })
                (assignment + basicAssignment.filterNot { it in assignment || -it in assignment }).toSet()
            else
                emptySet()
        }.filter { it.isNotEmpty() }.findAny().orElse(emptySet())

        println("c repetitions ${reps.get()}")
        //assert(ret.size == vars.size)
        return ret
    }
}

private object SCCBased2CNFSolver {
    internal fun simplifyClausesBasedOnValues(clauses: Collection<Clause>, assignment: Assignment) =
            clauses.filterNot { c -> c.any { it in assignment } }.map { c -> c.filterNot { -it in assignment } }

    internal sealed class Verdict {
        class Satisfied(val assignment: Assignment): Verdict()
        object Conflicts: Verdict()
        class Unresolved(val clauses: Collection<Clause>, val partialAssignment: Assignment): Verdict()
    }

    internal tailrec fun performBCP(clauses: Collection<Clause>, assignment: Assignment = emptySet()): Verdict {
        val valsFromUnitClauses = assignment + clauses.filter { it.size == 1 }.map { it[0] }.toSet()
        val simplifiedClauses = simplifyClausesBasedOnValues(clauses, valsFromUnitClauses)
        val newAssignment = assignment + valsFromUnitClauses
        return when {
            newAssignment.any { -it in newAssignment } || simplifiedClauses.any { it.isEmpty() } -> Verdict.Conflicts
            simplifiedClauses.isEmpty() -> Verdict.Satisfied(newAssignment)
            clauses.size != simplifiedClauses.size -> performBCP(simplifiedClauses, newAssignment)
            else -> Verdict.Unresolved(simplifiedClauses, newAssignment)
        }
    }

    fun solve(givenClauses: Collection<Clause>): Assignment {
        val verdict = performBCP(givenClauses.map { it.distinct() })
        return when (verdict) {
            is Verdict.Conflicts -> emptySet()
            is Verdict.Satisfied -> verdict.assignment
            is Verdict.Unresolved -> {
                val implGraph = DefaultDirectedGraph<Int, DefaultEdge>(DefaultEdge::class.java)
                verdict.clauses.forEach { c ->
                    c.forEach { implGraph.addVertex(it); implGraph.addVertex(-it) }
                    implGraph.addEdge(-c[0], c[1])
                    implGraph.addEdge(-c[1], c[0])
                }

                KosarajuStrongConnectivityInspector(implGraph).stronglyConnectedSets().run {
                    if (any { scc -> scc.any { -it in scc } }) {
                        emptySet()
                    } else {
                        reverse()
                        fold(verdict.partialAssignment) { acc, scc -> acc + scc.filter { -it !in acc && it !in acc } }
                                .sortedBy(::abs).toSet()
                    }
                }
            }
        }
    }
}

CommandLine().main(args)
